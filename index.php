<?php

use JeuDeLaVie\JeuDeLaVie;

require_once __DIR__ . DIRECTORY_SEPARATOR . 'autoload.php';

// Ici il faut lancer le jeu de la vie !
new JeuDeLaVie();