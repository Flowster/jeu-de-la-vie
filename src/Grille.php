<?php

namespace JeuDeLaVie;

class Grille
{
    /**
     * @var cellule[][]
     */
    private $grille = array();

    public function __construct($nb_ligne = 10, $nb_colonne = 10) {
        // 1) ici il faut initialiser le tableau grille à deux dimensions
        // Chaque cellule de ce tableau correspond à une ligne qui est lui même un tableau.
        // En utilisant les variables $nb_ligne et $nb_colonne et à l'aide d'une boucle for
        // crée le tableau multidimentionnel qui contiendra tes cellules
        // Dans chacune des cases de ce tableau, ajoutes un nouvel objet Cellule.
    }

    public function __toString() {
        // La méthode __toString() permet de fournir une représentation graphique d'un objet
        // Si l'on utilise par exemple `echo $grille` cette méthode sera automatiquement appelée
        // Ici tu dois donc pour chaque ligne afficher chaque cellule comme ci-dessous
        //    0      0
        //        0
        //  0       0
        //     0   000
        //    0    0 0
    }
}