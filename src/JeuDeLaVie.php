<?php

namespace JeuDeLaVie;

class JeuDeLaVie
{
    function run() {
        echo <<<EOL
Démarrage du jeu de la vie.
Attachez vos ceintures !
====
EOL;
        /* Nous allons dans un premier temps charger un objet Grille
         * Une grille contient des cellules
         * A chaque tour de jeu chaque cellule doit définir son nouvel état
         * Nous verrons plus loin l'algorithme qui définit cette mutation
         *
         * 1) Charges une nouvelle grille dans cette méthode run et affiches la.
         * charges le jeu de la vie via ton terminal en utilisant la commande `php index.php`
         *
         * 2) Crées une boucle infinie qui affiche la grille toutes les 2 secondes
         *
         * 3) Donnes vie à tes cellules !
         * Lorsque tu as construit ta grille dans la première étape, fournies à chaque cellule
         * une référence vers sa voisine via la méthode `addSibling`. Chaque cellule doit donc avoir
         * 8 voisines sauf celles se trouvant sur les bords
         * A chaque fois qu'on veut afficher une cellule, celle-ci doit intérroger ses voisines et leur
         * demander si elles sont vivantes ou mortes et redéfinir son état en fonction de l'algorithme
         * du jeu de la vie. On commence par la cellule en haut à gauche et on fini par la cellule en bas à droite
         *
         * 4) Houston nous avons un problème !
         * Lorsque nous parcourons les cellules et interrogeons les voisines nous n'obtenons pour les cellules
         * précédentes leur état à l'itération précédente mais leur état à cette nouvelle itération. Hors chaque cellule
         * doit définir son état suivant l'itération précédente. Trouves un moyen de contourner ce problème.
         *
         * 5) Il n'y a plus de bords !
         * Le tableau est maintenant un Tore (Torus) Les voisines des celulles du haut sont les cellules
         * Du bas du tableau, celles de droites sont celles de gauche et inversement.
         */
    }
}