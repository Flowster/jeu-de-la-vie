<?php

namespace JeuDeLaVie;

class Cellule
{
    /**
     * @var bool Cette cellule est-t-elle en vie ?
     */
    private $alive = false;

    /**
     * Constructeur de cellule
     */
    public function __construct() {
        // Lorsque l'on crée une cellule, il faut définir son état initial (vivante, morte)
        // Dans un premier temps, définies de manière aléatoire via la fonction rand()
        // l'état de cette cellule avec une chance sur 10 d'être vivante.
    }

    /**
     * Affichage d'une cellule
     */
    public function __toString() {
        // La méthode __toString() permet de fournir une représentation graphique d'un objet
        // Si l'on utilise par exemple `echo $cellule` cette méthode sera automatiquement appelée
        // Ici tu dois donc retourner un caractère "0" si la cellule est vivante ou un caractère vide "" si la cellule est morte
    }

    /**
     * @param Cellule $sibling
     */
    public function addSibling(Cellule &$sibling) {

    }

    /**
     * Doit retourner si la cellule est vivante ou morte
     * @return bool
     */
    public function isAlive() {

    }

    /**
     * La cellule évolue
     */
    public function evolve() {

    }
}