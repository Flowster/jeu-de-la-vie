<?php
spl_autoload_register(function ($className) {
    $classPaths = explode('\\', $className);
    if ($classPaths[0] === 'JeuDeLaVie') {
        $className = substr($className, strlen('JeuDeLaVie\\'));
        $file = __DIR__ . '\\src\\' . $className . '.php';
        $file = str_replace('\\', DIRECTORY_SEPARATOR, $file);
        if (file_exists($file)) {
            include($file);
        }
    }
});
